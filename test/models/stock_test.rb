require 'test_helper'

class StockTest < ActiveSupport::TestCase
  test 'year_value calculation' do
    stock = Stock.new(name: '111', unit_price: 1000, interest: 1.0, duration: 3)

    assert_equal stock.year_value(0), 1000
    assert_equal stock.year_value(1), 1010.05
    assert_equal stock.year_value(3), 1030.44
  end
end
