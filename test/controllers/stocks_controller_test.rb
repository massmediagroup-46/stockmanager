require 'test_helper'

class StocksControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @user = users(:one)
    @stock = stocks(:one)
    @stock2 = stocks(:two)
    sign_in @user
  end

  test 'should get index' do
    get stocks_url

    assert_response :success
  end

  test 'should get new' do
    get new_stock_url

    assert_response :success
  end

  test 'should create stock' do
    assert_difference('Stock.count') do
      post stocks_url, params: { stock: { duration: @stock.duration,
                                          interest: @stock.interest,
                                          name: @stock.name,
                                          unit_price_cents: @stock.unit_price_cents } }
    end

    assert_redirected_to stock_url(Stock.last)
  end

  test 'should show stock' do
    get stock_url(@stock)

    assert_response :success
  end

  test 'should not show alien stock' do
    get stock_url(@stock2)

    assert_equal 'Couldn\'t find the Stock', flash[:alert]
    assert_redirected_to stocks_url
  end

  test 'should get edit' do
    get edit_stock_url(@stock)

    assert_response :success
  end

  test 'should update stock' do
    patch stock_url(@stock), params: { stock: { duration: @stock.duration,
                                                interest: @stock.interest,
                                                name: @stock.name,
                                                unit_price_cents: @stock.unit_price_cents } }

    assert_redirected_to stock_url(@stock)
  end

  test 'should not update alien stock' do
    patch stock_url(@stock2), params: { stock: { duration: @stock.duration,
                                                 interest: @stock.interest,
                                                 name: @stock.name,
                                                 unit_price_cents: @stock.unit_price_cents } }

    assert_equal 'Couldn\'t find the Stock', flash[:alert]
    assert_redirected_to stocks_url
  end

  test 'should destroy stock' do
    assert_difference('Stock.count', -1) do
      delete stock_url(@stock)
    end

    assert_redirected_to stocks_url
  end

  test 'should not destroy alien stock' do
    assert_difference('Stock.count', 0) do
      delete stock_url(@stock2)
    end

    assert_equal 'Couldn\'t find the Stock', flash[:alert]
    assert_redirected_to stocks_url
  end
end
