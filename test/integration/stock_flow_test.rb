require 'test_helper'

class PostFlowTest < Capybara::Rails::TestCase
  include Warden::Test::Helpers

  def setup
    @user = users(:one)
  end

  def sign_in(user)
    login_as(user, scope: :user)
  end

  def sign_out
    logout(:user)
  end

  test 'guest user should see sign in' do
    visit root_path

    assert page.has_link?('Sign in')
  end

  test 'guest user should see sign up' do
    visit root_path

    assert page.has_link?('Sign up')
  end

  test 'guest user should see header' do
    visit root_path

    assert page.has_title?('Stock productivity calculator')
  end

  test 'logined user should see logout link' do
    sign_in @user
    visit stocks_path

    assert page.has_link?('Sign out')
  end

  test 'logined user should see Add Stock link' do
    sign_in @user
    visit stocks_path

    assert page.has_link?('Add Stock')
  end

  test 'logined user should see his stocks' do
    stock = stocks(:one)
    sign_in @user
    visit stocks_path

    assert page.has_content?(stock.name)
  end
end
