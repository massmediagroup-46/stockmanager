# README

## Stockmanager 
Is a Stock productivity calculator appliation with bar chart, that displays how Stock valuation changes over its duration in years.


#### *Details:*

##### Ruby version

2.5.1

##### System dependencies

* Install rvm, ruby, postgresql, docker, docker-compose

* Bundle install

##### Database creation

* updte database.yml
* rake db:create

##### Database initialization

* rake db:migrate

##### How to run the test suite

* rake

##### Deployment instructions

To start locally use command:
```sh
rails s
```
- browse `http://localhost:3000`


To start in docker use commands:
```sh
docker-compose build
docker-compose run web bundle exec rake db:create
docker-compose run web bundle exec rake db:migrate
docker-compose up
```
- browse `http://localhost:3000`

To stop your containers and remove the stopped containers use:
```sh
docker-compose down -v
```