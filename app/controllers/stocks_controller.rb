class StocksController < ApplicationController
  before_action :authenticate_user!
  before_action :set_stock, only: %i[show edit update destroy]

  def index
    # add will_paginate/caminari

    @stocks = []
    @stocks = current_user.stocks.page(params[:page]).per(2)
  end

  def show
    @chart_data = (0..@stock.duration).inject({}) do |result, year|
      result.merge(year => @stock.year_value(year))
    end
  end

  def new
    @stock = Stock.new
  end

  def edit; end

  def create
    @stock = current_user.stocks.new(stock_params)
    if @stock.save
      redirect_to @stock, notice: 'Stock was successfully created.'
    else
      render :new
    end
  end

  def update
    if @stock.update(stock_params)
      redirect_to @stock, notice: 'Stock was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    if @stock.destroy
      redirect_to stocks_url, notice: 'Stock was successfully destroyed.'
    else
      redirect_to stocks_url, alert: 'Can\'t destroy the stock.'
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_stock
    @stock = current_user.stocks.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    redirect_to stocks_url, alert: 'Couldn\'t find the Stock'
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def stock_params
    params.require(:stock).permit(:name, :unit_price, :unit_price_cents, :interest, :duration)
  end
end
