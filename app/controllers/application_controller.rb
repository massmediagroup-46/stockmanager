class ApplicationController < ActionController::Base
  protect_from_forgery

  def after_sign_out_path_for(_arg)
    root_path
  end

  def after_sign_in_path_for(_arg)
    stocks_path
  end
end
