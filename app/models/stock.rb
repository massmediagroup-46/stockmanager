class Stock < ApplicationRecord
  INTERST_COMPOUNDED_PER_YEAR = 12

  belongs_to :user

  validates :name, presence: true
  validates :unit_price_cents, presence: true, numericality: true
  validates :interest, presence: true, numericality: true
  validates :duration, presence: true, numericality: true

  monetize :unit_price_cents

  def year_value(year)
    (
    unit_price_dollars *
        (1 + interest / 100 / INTERST_COMPOUNDED_PER_YEAR)**(INTERST_COMPOUNDED_PER_YEAR * year)
  ).round(2)
  end

  def unit_price_dollars
    unit_price_cents / 100
  end
end
