class CreateStocks < ActiveRecord::Migration[5.2]
  def change
    create_table :stocks do |t|
      t.string :name
      t.integer :unit_price_cents
      t.float :interest
      t.integer :duration
      t.integer :user_id

      t.timestamps
    end
  end
end
